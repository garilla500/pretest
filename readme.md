## Spesifikasi
- Aplikasi menggunakan framework laravel 4.3

## About Pretest

### - Untuk Library / Helper dibuat di direktori Helper/convertnumber/abbreviator
dengan url test /pretest/{nominal}
contoh http://127.0.0.1/pretest/1000

### - Untuk parsing data txt ke mysql menggunakan 1 function, dengan controller yang sama dengan Library
dengan endpoint /insert_data contoh http://127.0.0.1/insert_data/

### - Untuk API pencarian dengan response JSON
dengan endpoint /api/tasks/{BRANCHFULLNAME} contoh http://127.0.0.1/api/tasks/AMBON dan menggunakan method GET.
*Note---- untuk handle response error tidak dibuat

### - Konsep Microservice
file berupa pdf ada di source dengan nama file microservice.pdf