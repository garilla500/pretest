<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\DB;
use App\Task;
use App\Transformer\TaskTransformer;

class TaskController extends Controller
{
	protected $response;
 
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function index($name)
    {
        //Get all task
        $tasks = DB::table('branch')->where('BRANCHFULLNAME', $name)->get();
        return [
            'rsp_desc' => 'success',
            'rsp_code' => '01',
            'data' => $tasks
        ];
    }
}
