<?php

namespace Convertnumber\Abbreviator;

use Illuminate\Support\ServiceProvider;

class AbbreviatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'abbreviator');
        $this->publishes([
        __DIR__.'/views' => base_path('resources/views/convertnumber/abbreviator'),
    ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Convertnumber\Abbreviator\AbbreviatorController');
    }
}
