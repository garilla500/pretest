<?php

namespace Convertnumber\Abbreviator;
 
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
 
class AbbreviatorController extends Controller
{
 
    public function index($nilai)
    {
    	switch ($nilai) {
    		case '1000':
    		$nilai = "1k";
    		break;
	    	case '10000':
	    		$nilai = "10k";
	    		break;
	    	case '100000':
	    		$nilai = "100k";
	    		break;
	    	case '1000000':
	    		$nilai = "1m";
	    		break;
    	}
    	return view('abbreviator::abbreviator', compact('nilai'));
    }

    public function insert_data($nilai)
    {
    	$lines = Storage::disk('public')->get('CabangKonvent.txt');
    	$linesList = explode(PHP_EOL, $lines);
		foreach(array_slice($linesList,1) as $line)
		{
	   		$words = explode('|',$line);
	   		//echo "<pre>", var_dump($words), "</pre>";
	   		/*DB::table('branch')->insert([
			  [
			    'BRANCHID' => $words[0], 
			    'CGID' => $words[1],
			    'BRANCHFULLNAME' => $words[2],
			    'BRANCHINITIALNAME' => $words[3],
			    'COMPANYID' => $words[4],
			    'BRANCHADDRESS' => $words[5],
			    'BRANCHRT' => $words[6],
			    'BRANCHRW' => $words[7],
			    'BRANCHKELURAHAN' => $words[8],
			    'BRANCHKECAMATAN' => $words[9],
			    'BRANCHCITY' => $words[10],
			    'BRANCHZIPCODE' => $words[11],
			    'BRANCHAREAPHONE1' => $words[12],
			    'BRANCHPHONE1' => $words[13],
			    'BRANCHAREAPHONE2' => $words[14],
			    'BRANCHPHONE2' => $words[15],
			    'BRANCHAREAFAX' => $words[16],
			    'BRANCHFAX' => $words[17],
			    'CONTACTPERSONNAME' => $words[18],
			    'CONTACTPERSONJOBTITLE' => $words[19],
			    'CONTACTPERSONEMAIL' => $words[20],
			    'CONTACTPERSONHP' => $words[21],
			    'BRANCHMANAGERNAME' => $words[22],
			    'ADHNAME' => $words[23],
			    'ARCONTROLNAME' => $words[24],
			    'ASSETDOCCUSTODIANNAME' => $words[25],
			    'BRANCHSTATUS' => $words[26],
			    'ISCASHIERCLOSED' => $words[27],
			    'ISHEADOFFICE' => $words[28],
			    'ACCOUNTNAME' => $words[29],
			    'ACCOUNTNO' => $words[30],
			    'ACCOUNTNAME2' => $words[31],
			    'ACCOUNTNO2' => $words[32],
			    'AREAID' => $words[33],
			    'LEGALDEPTNAME' => $words[34],
			    'BANKBRANCH' => $words[35],
			    'BANKNAME' => $words[36],
			    'ISOUTSOURCECALLINGCENTER' => $words[37],
			    'ISOUTSOURCEPRINTWL' => $words[38],
			    'ISUPDATECGID' => $words[39],
			    'USRUPD' => $words[40],
			    'DTMUPD' => $words[41],
			    'SANDIBI1' => $words[42],
			    'SANDIBI2' => $words[43],
			    'PRODUCTIVITYCOLLECTION' => $words[44],
			    'PRODUCTIVITYVALUE' => $words[45],
			    'NUMOFEMPLOYEE' => $words[46],
			    'BANKNAME2' => $words[47],
			    'BANKBRANCH2' => $words[48],
			    'KPPID' => $words[49],
			  ]
			]);*/
	    }
    }
 
}
